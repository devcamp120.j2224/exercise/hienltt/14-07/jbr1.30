package com.devcamp;

public class Employee {
    int id;
    String firstName;
    String lastName;
    int salary;

    public Employee(int id, String firstName, String lastName, int salary){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }
    public int getId() {
        return id;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public String getName(){
        return "full name: " + this.firstName + " " + this.lastName;
    }
    public int getAnualSalary(){
        return this.salary * 12;
    }
    public int raiseSalary(int percent){
        return this.salary + (this.salary * percent / 100);
    }
    @Override
    public String toString(){
        return "Employee[id= " + this.id + ", nam= " + this.firstName + " " + this.lastName + ", salary= " + this.salary + "]";
    }
}
