import com.devcamp.Employee;

public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1, "Nguen Van", "A", 10000000);
        Employee employee2 = new Employee(2, "Nguen Van", "B", 12000000);

        System.out.println(employee1.toString());
        System.out.println(employee2.toString());

        System.out.println("Thông tin sau khi tăng lương:");

        System.out.println("employee1: " + employee1.raiseSalary(10));
        System.out.println("employee2: " + employee2.raiseSalary(20));
    }
}
